#include "mbed.h"

Serial pc(USBTX, USBRX); // tx, rx

PwmOut myled1(LED1);
PwmOut myled2(LED2);
PwmOut myled3(LED3);
PwmOut myled4(LED4);

InterruptIn echo(p7);
InterruptIn pb(p8);

DigitalOut gpio30(p30); //p30
DigitalOut gpio29(p29); //p29
DigitalOut gpio28(p28); //p28
DigitalOut gpio27(p27); //p27

DigitalOut trigger(p6);

Timer sonar;

void MyUltrasonicSensor(float sensor_angle) {

  int my_distance = 0;
  int my_correction = 0;

  trigger = 1;
  sonar.reset();
  wait_us(10.0);
  trigger = 0;
  //wait for echo high
  while (echo==0) {};
  //echo high, so start timer
  sonar.start();
  //wait for echo low
  while (echo==1) {};
  //stop timer and read value
  sonar.stop();
  //subtract software overhead timer delay and scale to inches
  my_distance = (sonar.read_us()-my_correction)/148.0;
    //wait so that any echo(s) return before sending another ping
  //wait(0.02);

  if ( my_distance <= 144 ) {
  pc.printf("\n");
  pc.printf("MO_INFO: Found an object %d inches away, %f degrees from center\n",my_distance,sensor_angle);
  pc.printf("\n");
  }

}

int main() {

  pc.printf("\n");
  pc.printf("\n");
  pc.printf("\n");
  pc.printf("                        MM                                  \n");
  pc.printf("                      MMMMMM                                \n");
  pc.printf("                     MMMMMMMM      MM                       \n");
  pc.printf("                    MMMMMMMMMM   MMMMMM                     \n");
  pc.printf("                   MMMMMMMMMMMMMMMMMMMMMM                   \n");
  pc.printf("                  MMMMMMMMMMMMMMMMMMMMMMMM                  \n");
  pc.printf("                  MMMMMMMMMMMMMMMMMMMMMMMM                  \n");
  pc.printf("                 MMMMMMMMMM      MMMMMMMMMM                 \n");
  pc.printf("                MMMMMMMMM          MMMMMMMMM                \n");
  pc.printf("                MMMMMMMM     MM     MMMMMMMM		 \n");
  pc.printf("               MMMMMMMMM    MMMM    MMMMMMMMM		 \n");
  pc.printf("              MMMMMMMMMM    MMMM    MMMMMMMMMM		 \n");
  pc.printf("              MMMMMMMMMM    MMMM    MMMMMMMMMM		 \n");
  pc.printf("             MMMMMMMMMMM    MMMM    MMMMMMMMMMM		 \n");
  pc.printf("            MMMMMMMMMMMM    MMMM    MMMMMMMMMMMM		 \n");
  pc.printf("            MMMMMMMMMMMM     MM     MMMMMMMMMMMM		 \n");
  pc.printf("           MMMMMMMMMMMMMM          MMMMMMMMMMMMMM           \n");
  pc.printf("         MMMMMMMMMMMMMMMMMM      MMMMMMMMMMMMMMMMMM         \n");
  pc.printf("        MMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMM        \n");
  pc.printf("       MMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMM       \n");
  pc.printf("      MMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMM      \n");
  pc.printf("\n");
  pc.printf("\n");
  pc.printf("               Mount Olympus Code Initializing              \n");
  pc.printf("        Brought to you by Olu Afolayan and Dan Taylor       \n");
  pc.printf("                 WES237A - Assignment 1.8                   \n");
  pc.printf("\n");

  int count=0;
  int old_count=0;

  // Use internal pullup for pushbutton
  pb.mode(PullUp);

  // Delay for initial pullup to take effect
  wait(.001);

  int old_pb;
  int new_pb;
  int new_pb_wait;

  int rot_angle;
  int rot_dir;

  //int correction = 0;

  char c ;
  char last_c;

  sonar.reset();
  // measure actual software polling timer delays
  // delay used later in time correction
  // start timer
  sonar.start();
  // min software polling delay to read echo pin
  while (echo==2) {};
  // stop timer
  sonar.stop();
  // read timer
  //correction = sonar.read_us();
  //printf("MO_INFO: Approximate software overhead timer delay is %d uS\n",correction);

  pc.printf("\n");
  pc.printf("MO_INFO: Step1 : Position the radar station...\n");
  pc.printf("MO_INFO: --> Press 'f' to rotate the radar station clockwise\n");
  pc.printf("MO_INFO: --> Press 'r' to rotate the radar station counter-clockwise\n");
  pc.printf("MO_INFO: --> Press 's' to stop the radar station in it's current location\n");
  pc.printf("\n");

  pc.printf("\n");
  pc.printf("MO_INFO: Step2 : Press the push button to begin radar operation\n");
  pc.printf("MO_INFO: Step3 : Press the push button again to pause radar operation\n");
  pc.printf("MO_INFO: Step4 : Press the push button again to resume radar operation\n");
  pc.printf("\n");

  while(1) {

    new_pb = pb;

    if (pc.readable()) {
      c = pc.getc();
      last_c = c;
      if ( last_c == 'f') {
          rot_angle=1; rot_dir=0;
      }
      if ( last_c == 'r') {
          rot_angle=1; rot_dir=1;
      }
      if ( last_c == 's') {
         rot_angle=1; rot_dir=0;
      }
      if ( last_c == 'e') {
         rot_angle=1; rot_dir=0;
      }
    }

    if ( new_pb == 0 && old_pb == 1 ) {

      // Push button needs to be held for 0.005
      wait(.005);

      new_pb_wait = pb;

      if ( new_pb_wait == 0 ) {

        pc.printf("\n");
        pc.printf("MO_INFO: Pushbutton Pressed\n");
        pc.printf("\n");

        while (pb==0) {

          // Waiting for user to release push button

        }

        if ( count == 0 ) {

          count = 1; myled4 = 0; myled3 = 0; myled2 = 0; myled1 = 1; new_pb=1;

        } else if ( count == 1 ) {

          count = 0; myled4 = 0; myled3 = 0; myled2 = 0; myled1 = 0; new_pb=1;

        } else {

          count = 0; myled4 = 0; myled3 = 0; myled2 = 0; myled1 = 0; new_pb=1;

        }

        //pc.printf("MO_INFO: count %d old_count %d old_pb %d new_pb %d pb %f myled4 %f myled3 %f myled2 %f myled1 %f\n",count,old_count,old_pb,new_pb,pb.read(),myled4.read(),myled3.read(),myled2.read(),myled1.read());

      }

    }

    if ( count != old_count && count == 1 ) {

      pc.printf("\n");
      pc.printf("MO_INFO: Press push button to pause the radar\n");
      pc.printf("\n");

    }

    if ( count != old_count && count == 0 ) {

      pc.printf("\n");
      pc.printf("MO_INFO: Press push button to resume the radar\n");
      pc.printf("\n");

    }

    if ( last_c == 'f') {

        gpio30=1; gpio29=0; gpio28=0; gpio27=0; wait(.005); //stepper_position = 1
        gpio30=1; gpio29=1; gpio28=0; gpio27=0; wait(.005); //stepper_position = 2
        gpio30=0; gpio29=1; gpio28=0; gpio27=0; wait(.005); //stepper_position = 3
        gpio30=0; gpio29=1; gpio28=1; gpio27=0; wait(.005); //stepper_position = 4
        gpio30=0; gpio29=0; gpio28=1; gpio27=0; wait(.005); //stepper_position = 5
        gpio30=0; gpio29=0; gpio28=1; gpio27=1; wait(.005); //stepper_position = 6
        gpio30=0; gpio29=0; gpio28=0; gpio27=1; wait(.005); //stepper_position = 7
        gpio30=1; gpio29=0; gpio28=0; gpio27=1; wait(.005); //stepper_position = 8

        if ( rot_angle < 512 ) {
          rot_angle++;
        } else {
          rot_angle=1;
        }

    }

    if ( last_c == 'r') {

        gpio30=1; gpio29=0; gpio28=0; gpio27=0; wait(.005); //stepper_position = 1
        gpio30=1; gpio29=0; gpio28=0; gpio27=1; wait(.005); //stepper_position = 8
        gpio30=0; gpio29=0; gpio28=0; gpio27=1; wait(.005); //stepper_position = 7
        gpio30=0; gpio29=0; gpio28=1; gpio27=1; wait(.005); //stepper_position = 6
        gpio30=0; gpio29=0; gpio28=1; gpio27=0; wait(.005); //stepper_position = 5
        gpio30=0; gpio29=1; gpio28=1; gpio27=0; wait(.005); //stepper_position = 4
        gpio30=0; gpio29=1; gpio28=0; gpio27=0; wait(.005); //stepper_position = 3
        gpio30=1; gpio29=1; gpio28=0; gpio27=0; wait(.005); //stepper_position = 2

        if ( rot_angle == 1 ) {
          rot_angle=512;
        } else {
          rot_angle--;
        }

    }

    if ( last_c == 's' ) {

      // Stop in this position

    }

    if ( count == 1 ) {

        if ( rot_angle <= 257 && rot_dir == 0  ) {

          gpio30=1; gpio29=0; gpio28=0; gpio27=0; wait(.002); //stepper_position = 1
          gpio30=1; gpio29=1; gpio28=0; gpio27=0; wait(.002); //stepper_position = 2
          gpio30=0; gpio29=1; gpio28=0; gpio27=0; wait(.002); //stepper_position = 3
          gpio30=0; gpio29=1; gpio28=1; gpio27=0; wait(.002); //stepper_position = 4
          gpio30=0; gpio29=0; gpio28=1; gpio27=0; wait(.002); //stepper_position = 5
          gpio30=0; gpio29=0; gpio28=1; gpio27=1; wait(.002); //stepper_position = 6
          gpio30=0; gpio29=0; gpio28=0; gpio27=1; wait(.002); //stepper_position = 7
          gpio30=1; gpio29=0; gpio28=0; gpio27=1; wait(.002); //stepper_position = 8

          if (rot_angle==16) { MyUltrasonicSensor(-78.75); }
          if (rot_angle==48) { MyUltrasonicSensor(-56.25); }
          if (rot_angle==80) { MyUltrasonicSensor(-33.75); }
          if (rot_angle==112) { MyUltrasonicSensor(-11.25); }
          if (rot_angle==144) { MyUltrasonicSensor(11.25); }
          if (rot_angle==176) { MyUltrasonicSensor(33.75); }
          if (rot_angle==208) { MyUltrasonicSensor(56.25); }
          if (rot_angle==240) { MyUltrasonicSensor(78.75); }

          rot_angle++;

        } else if ( rot_angle <= 257 && rot_dir == 1 ) {

          gpio30=1; gpio29=0; gpio28=0; gpio27=0; wait(.002); //stepper_position = 1
          gpio30=1; gpio29=0; gpio28=0; gpio27=1; wait(.002); //stepper_position = 8
          gpio30=0; gpio29=0; gpio28=0; gpio27=1; wait(.002); //stepper_position = 7
          gpio30=0; gpio29=0; gpio28=1; gpio27=1; wait(.002); //stepper_position = 6
          gpio30=0; gpio29=0; gpio28=1; gpio27=0; wait(.002); //stepper_position = 5
          gpio30=0; gpio29=1; gpio28=1; gpio27=0; wait(.002); //stepper_position = 4
          gpio30=0; gpio29=1; gpio28=0; gpio27=0; wait(.002); //stepper_position = 3
          gpio30=1; gpio29=1; gpio28=0; gpio27=0; wait(.002); //stepper_position = 2

          if (rot_angle==16) { MyUltrasonicSensor(-78.75); }
          if (rot_angle==48) { MyUltrasonicSensor(-56.25); }
          if (rot_angle==80) { MyUltrasonicSensor(-33.75); }
          if (rot_angle==112) { MyUltrasonicSensor(-11.25); }
          if (rot_angle==144) { MyUltrasonicSensor(11.25); }
          if (rot_angle==176) { MyUltrasonicSensor(33.75); }
          if (rot_angle==208) { MyUltrasonicSensor(56.25); }
          if (rot_angle==240) { MyUltrasonicSensor(78.75); }

          rot_angle--;

        } else {

          // Pause in this position

        }

        if (rot_angle==1) { rot_dir=0;}
        if (rot_angle==257) { rot_dir=1;}

    }

    old_pb = new_pb;
    old_count = count;

  }
}
